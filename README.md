# Template für Projektseminar-Bericht

Autor: Jannes Bantje, j.bantje@wwu.de

## Kompillieren des Berichts

Um dieses komplexe LaTeX-Dokument kompillieren zu können, sind einige Vorarbeiten nötig – andernfalls hagelt es Fehlermeldungen!

* Da einige eher neue Pakete benutzt werden, sollte eine aktuelle TeX-Distribution installiert sein (getestet mit TeXlive 2016).
	* Ich lege allen Linux-Usern nahe, TeXlive _nicht_ über den Paketmanager zu installieren, sondern wie [HIER](https://www.tug.org/texlive/quickinstall.html) erläutert. Anpassend des `PATH`  und ggf. `purge` des alten Paketmanager-TeXlive nicht vergessen – auf ersteres weißt der Installer aber auch ausdrücklich hin!
	* Unter OS X führt man einfach den MacTeX-Installer aus und ist glücklich. (MacTeX ist TeXlive + ein paar OS X-Besonderheiten)
	* Für Windows ist es bei Benutzung von MikTeX wegen des chaotischen Updateprozesses am einfachsten einfach MikTeX frisch zu installieren, um die TeX-Distribution zu aktualisieren. Für TeXlive unter Windows gibt es nichts besonderes zu beachten afaik.
* Für das Syntax-Highlighting wird ein externes Python-Programm namens `Pygments` eingesetzt. Dieses muss per Hand separat installiert werden! 
	* Python muss mindestens in Version 2.6 installiert sein: Test mittels `python --version`
	* Pygments lässt sich nun auf verschiedenen Wegen installieren: Unter OS X hat `sudo easy_install Pygments` zuverlässig funktioniert. Selbstverständlich ist dies auch mittels `pip install Pygments` möglich, sofern PIP installiert ist. Weitere Hinweise findet man in der Dokumentation des Pakets `minted`, welches sich um die Benutzung von `Pygments` aus LaTeX heraus kümmert. Auf StackExchange findet man sonst auch immer schnell Hilfe…
* Das Dokument ist für den Einsatz mit `pdflatex` – also dem Standard-Kompiler – optimiert!
* Unter anderem für das Syntax-Highlighting ist die die `--shell-escape`-Option von `pdflatex` notwendig! Alle Dateien sind mit speziellen Kommentaren versehen, die _einige_ Editoren dazu veranlassen, diese Flag automatisch zu setzen. Bei jedem halbwegs für LaTeX geeigneten Editor sollte es aber auch die Möglichkeit geben, `shell-escape` durch einen Abstecher in die Einstellungen zu aktivieren.

Sollte es weiterhin Probleme mit `minted` bzw. `Pygments` geben, _könnte_ es noch sein, dass der Editor aus irgendeinem Grund nicht den `PATH` des aktuellen Benutzer nimmt und dann eventuell `Pygments` bzw. das eigentlich Programm `pygmentize` findet.
