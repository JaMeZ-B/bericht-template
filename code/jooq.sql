SELECT * FROM tabLike
    INNER JOIN tabUser
    ON (tabLike.userId = tabUser.userId)
    WHERE (tabLike.productId = 1337)
    AND (tabUser.clusterId = 42)
    ORDER BY tabLike.idLike ASC;